#! /usr/bin/env python3

import subprocess

ROOT='/home/tfheen/debian/dsa-wiki'
IMAGE='docker.io/debian:testing'

def main():
# maybe: podman pull docker.io/debian:testing
# XXX: ensure /tmp/foo exists
    subprocess.check_call(['podman', 'run', '-it',
                           '-v', '{}:/build'.format(ROOT),
                           '-v','/home/tfheen/debian/dsa-container-builder:/dsa-build',
                           '-v', '/tmp/foo:/var/lib/containers',
                           '--device', '/dev/fuse',
                           '--env-file', 'build-env',
                           IMAGE,
                           "/bin/sh", "-c", "/dsa-build/prep;/build/deborg-build-container"])

if __name__ == '__main__':
    main()
